from django.conf.urls import url, include
from . import views
from .views import MorceauDetailView, MorceauDeleteView, MorceauUpdateView, ArtisteDetailView, ArtisteListView, ArtisteDeleteView, ArtisteUpdateView, MorceauListView, MorceauCreateView, home, ArtisteCreateView


app_name = 'musiques'
urlpatterns = [
    url(r'^home$', home, name='home'),
    url(r'^detailArtiste/(?P<pk>\d+)$',
        ArtisteDetailView.as_view(), name='artiste-detail'),
    url(r'^listArtiste$', ArtisteListView.as_view(), name='artiste-list'),
    url(r'^addArtiste$', ArtisteCreateView.as_view(), name="artiste-add"),
    url(r'^delArtiste/(?P<pk>\d+)$',
        ArtisteDeleteView.as_view(), name='artiste-del'),
    url(r'^updateArtiste/(?P<pk>\d+)$',
        ArtisteUpdateView.as_view(), name='artiste-update'),
    url(r'^detailMorceau/(?P<pk>\d+)$',
        MorceauDetailView.as_view(), name='morceau-detail'),
    url(r'^listMorceau$', MorceauListView.as_view(), name='morceau-list'),
    url(r'^addMorceau$', MorceauCreateView.as_view(), name='morceau-add'),
    url(r'^delMorceau/(?P<pk>\d+)$',
        MorceauDeleteView.as_view(), name='morceau-del'),
    url(r'^updateMorceau/(?P<pk>\d+)$',
        MorceauUpdateView.as_view(), name='morceau-update'),
]
