from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse
from django.views.generic import DetailView, ListView, CreateView, DeleteView, UpdateView
#from django.views.generic.list import ListView
#from django.views.generic.edit import CreateView
#from django.views.generic.delete import DeleteView
from django.forms import ModelForm
from .models import Morceau, Artiste
from django.views.generic.edit import DeleteView


# Create your views here.


def home(request):
    return render(request, 'musiques/home.html')


class ArtisteDetailView(DetailView):
    model = Artiste
    template_name = 'musiques/artiste.html'


class ArtisteListView(ListView):
    model = Artiste

    def get_context_data(self, **kwargs):
        context = super(ArtisteListView, self).get_context_data(**kwargs)
        context['template'] = "musiques/listArtiste.html"
        return context


class ArtisteCreateView(CreateView):
    model = Artiste
    fields = ['nom']

    def get_success_url(self):
        return reverse('musiques:artiste-list')


class ArtisteDeleteView(DeleteView):
    model = Artiste

    def get_success_url(self):
        return reverse('musiques:artiste-list')


class ArtisteUpdateView(UpdateView):
    model = Artiste
    fields = ['nom']

    def get_success_url(self):
        return reverse('musiques:artiste-list')


class MorceauDetailView(DetailView):
    model = Morceau
    template_name = 'musiques/morceau.html'


class MorceauListView(ListView):
    model = Morceau

    def get_context_data(self, **kwargs):
        context = super(MorceauListView, self).get_context_data(**kwargs)
        context['template'] = 'musiques/listemorceau.html'
        return context


class MorceauCreateView(CreateView):
    model = Morceau
    fields = ['artiste', 'titre']

    def get_success_url(self):
        return reverse('musiques:morceau-list')

    def get_success_url(self):
        return reverse('musiques:morceau-list')


class MorceauUpdateView(UpdateView):
    model = Morceau
    fields = ['artiste', 'titre']

    def get_success_url(self):
        return reverse('musiques:morceau-list')

    def get_success_url(self):
        return reverse('musiques:morceau-list')


class MorceauDeleteView(DeleteView):
    model = Morceau

    def get_success_url(self):
        return reverse('musiques:morceau-list')
