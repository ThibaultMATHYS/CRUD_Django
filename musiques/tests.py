from django.test import TestCase
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch

from musiques.models import Morceau, Artiste

# Create your tests here.


class MorceauTestCase(TestCase):
    def setUp(self):
        Morceau.objects.create(titre='musique1')
        Morceau.objects.create(titre='musique2')
        Morceau.objects.create(titre='musique3')

    def test_morceau_url_name(self):
        try:
            url = reverse('musiques:morceau-detail', args=[1])
        except NoReverseMatch:
            assert False

    def test_morceau_url(self):
        morceau = Morceau.objects.get(titre='musique1')
        url = reverse('musiques:morceau-detail', args=[morceau.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_add_bd(self):
        assert Morceau.objects.filter(pk=1).exists()

    def test_update_db(self):
        Morceau.objects.filter(pk=1).update(titre="testUpdateDb")
        morceau = Morceau.objects.get(pk=1)
        assert morceau.titre == 'testUpdateDb'

    def test_delete_db(self):
        assert Morceau.objects.filter(pk=1).delete()


class ArtisteTestCase(TestCase):
    def setUp(self):
        Artiste.objects.create(nom='artiste1')
        Artiste.objects.create(nom='artiste2')
        Artiste.objects.create(nom='artiste3')

    def test_artiste_url_name(self):
        try:
            url = reverse('musiques:artiste-detail', args=[1])
        except NoReverseMatch:
            assert False

    def test_artiste_url(self):
        artiste = Artiste.objects.get(nom='artiste1')
        url = reverse('musiques:artiste-detail', args=[artiste.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_add_bd(self):
        assert Artiste.objects.filter(pk=1).exists()

    def test_update_db(self):
        Artiste.objects.filter(pk=1).update(nom="testUpdateDb")
        artiste = Artiste.objects.get(pk=1)
        assert artiste.nom == 'testUpdateDb'

    def test_delete_db(self):
        assert Artiste.objects.filter(pk=1).delete()
